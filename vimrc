" shared settings form vim

set nocompatible
set path+=**
filetype plugin indent on
" dont create backup files (file~) 
set nobackup
set writebackup
set noundofile
" syntax highlighting for dark background
syntax on
" List of filetypes to highlight, in asciidoctor codeblocks
let g:asciidoctor_fenced_languages = ['python', 'bash', 'perl', 'haskell']
" same for markdown
let g:markdown_fenced_languages = ['sh', 'python', 'perl']
" dark Background
set bg=dark
" relative line numbers with absolut number of current line
" (Hybrid Numbering)
" nur absolute Nummerierung im Insert Mode
" https://jeffkreeftmeijer.com/vim-number/
set number relativenumber
augroup numbertoggle
  autocmd!
  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
augroup END
" Highlight searches (use <C-L> to temporarily turn off highlighting; 
" see the mapping of <C-L> below)
set hlsearch

" Better command-line completion
set wildmenu
" Show partial commands in the last line of the screen
set showcmd
" When opening a new line and no filetype-specific indenting is enabled,
" keep the same indent as the line you're currently on. Useful for READMEs,
" etc.
set autoindent
" Enable use of the mouse for all modes
"set mouse=a
set shiftwidth=4
set softtabstop=4
set tabstop=4
" Indentation settings for using 4 spaces instead of tabs.
"set expandtab

" Key Bindings
"
" Insert Mode: Jump to End of Line with ctrl-e
inoremap <C-e> <C-o>$

" Plugins (via vim-plug)
call plug#begin('~/.vim/plugged')
" fuzzyfinder
Plug 'junegunn/fzf.vim'
" easy comments
Plug 'scrooloose/nerdcommenter'
" git integration
Plug 'tpope/vim-fugitive'
" easy quotation marks
Plug 'tpope/vim-surround'
" autoclose brackets
"Plug 'townk/vim-autoclose'
Plug 'jiangmiao/auto-pairs'
" AsciiDoctor Plugin
Plug 'habamax/vim-asciidoctor'
" Lineup Text
Plug 'godlygeek/tabular'
" Snippets
Plug 'honza/vim-snippets'
Plug 'sirver/ultisnips'
" Everforest theme
Plug 'sainnhe/everforest'
call plug#end()

" vim:filetype=vim
