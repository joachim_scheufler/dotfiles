## Some Aliases

# asi - apt search install
alias asi="sudo apt install \$(apt-cache search . | fzf-tmux | awk '{print \$1}')"
# se - search and edit in current path
alias se="vim \$(fdfind -t f | fzf-tmux)"
# backuptraffic - search journal for ip traffic from backup service
# alias backuptraffic='journalctl -u backup.service MESSAGE_ID=ae8f7b866b0347b9af31fe1c80b127c0 | grep Received | cut -d" " -f 1-3,7-'
alias backuptraffic='journalctl -u backup.service MESSAGE_ID=ae8f7b866b0347b9af31fe1c80b127c0 | grep received | cut -d" " -f 1-3,12-'

## FZF (Fuzzyfind) Integration
source /usr/share/doc/fzf/examples/key-bindings.bash
#source /usr/share/doc/fzf/examples/completion.bash

# https://medium.com/free-code-camp/fzf-a-command-line-fuzzy-finder-missing-demo-a7de312403ff
# Exclude those directories even if not listed in .gitignore, or if .gitignore
# is missing
FD_OPTIONS="--follow --exclude .git --exclude node_modules"

# Change behavior of fzf dialogue
export FZF_DEFAULT_OPTS="--no-mouse --height 50% -1 --reverse --multi --inline-info --preview='[[ \$(file --mime {}) =~ binary ]] && echo {} is a binary file || (batcat --style=numbers --color=always {} || cat {}) 2> /dev/null | head -300' --preview-window='right:hidden:wrap' --bind='f3:execute(bat --style=numbers {} || less -f {}),f2:toggle-preview,ctrl-d:half-page-down,ctrl-u:half-page-up,ctrl-a:select-all+accept,ctrl-y:execute-silent(echo {+} | xsel --clipboard --input)'"

# Change find backend
# Use 'git ls-files' when inside GIT repo, or fd otherwise
# export FZF_DEFAULT_COMMAND="git ls-files --cached --others --exclude-standard | fd --type f --type l $FD_OPTIONS"

# Find commands for "Ctrl+T" and "Alt+C" shortcuts
# ctrl-t: find files
# alt+c:  cd to directory
export FZF_CTRL_T_COMMAND="fdfind $FD_OPTIONS"
export FZF_ALT_C_COMMAND="fdfind --type d $FD_OPTIONS"

# Fuck!
# https://github.com/nvbn/thefuck
if [ -x /usr/bin/thefuck ]; then
	eval $(thefuck --alias)
fi

# go #num directories up
# up 2
up() { cd $(eval printf '../'%.0s {1..$1}); }

# mkdir & cd
mkcd () { mkdir -p "$1" && cd "$1"; }

# vi:filetype=bash
